# HLW8012

HLW8012 library for Arduino and ESP8266 using the Arduino Core for ESP8266.

This **was** the official repository for HLW8012 library until April 10th, 2018, when it was moved to GitHub.
Please visit the new repository to checkout the latest version of the firmware, download up to date images, read the documentation, report issues or file pull-requests.

> https://github.com/xoseperez/hlw8012
